# 8. Setting White Balance

![](images/image15.png)

## Some of my pictures are too yellow. Some are too blue. What is the problem and what can I do about it?

---

*Time to complete: 5-10 minutes*

In this lesson you will learn:

* What white balance means
* How to adjust color with white balance control

---

Your eyes, in combination with your brain, do an excellent job of calibrating how you perceive color. You will recognize a color as red even if the light source illuminating it is very yellow, making the apparent color orange. You will recognize fleshtones even if the light illuminating the subject is a greenish-blue fluorescent.

A digital camera can’t do that. It has to be calibrated to adjust for different light sources’ color temperature. This calibration is measured against absolute white. That’s why it is called white balance.

---

![](images/image12.png)

*An example of a subject lit by a tungsten bulb*

Tungsten bulbs have a very warm light temperature, coloring images more yellow. Daylight on an overcast, cloudy day has a very cool light temperature, coloring the image blue. Other light sources — sunlight, fluorescent light, shade — all fall in the middle. Color temperature has nothing to do with the amount of light.

---

Good color management is important. It makes an image much more professional in appearance. Many smartphones provide a variety of presets for white balance.

![](images/image16.png)

*Sample icons for the common white balance options.*

If your mobile device allows you to set the white balance manually, do it. Your smartphone may have a button or menu option for white balance. It will offer several icons representing five or six common light sources. Pick the one that best represents the shooting conditions you are working under.

If your smartphone cannot control white balance manually, you can correct color with post-production apps (for more on this, see the section on post-production). If the color temperature is way off, it is VERY IMPORTANT to make adjustments before publishing your images.

---

## AWB (auto white balance)

![A common Auto White Balance icon](images/image17.png "A common Auto White Balance icon")

*A common icon for the auto white-balance setting*

Use this setting for shooting anywhere. The mobile device does its best to guess what the right setting should be, but it can provide poor results if the camera guesses wrong. This is the setting you should use if you’re unsure of which setting is the best choice.

---

## Daylight

![](images/image14.png)

*A common icon for the daylight white-balance setting*

Use this setting for shooting outdoors. This setting will give your images a very “cool” or slight blue tint. When shooting indoors, this setting will give your images an orange tint.

---

## Cloudy

![](images/image13.png)

*A common icon for the cloudy white-balance setting*

Use this setting for shooting outdoors. This setting will give your images a slightly “warmer” color. While the daylight option is technically the more correct white balance, using the cloudy setting creates a very appealing image. When shooting indoors, this setting will give your images a strong orange tint.

---

## Incandescent

![](images/image06.png)

*A common icon for the incandescent white-balance setting*

Use this setting for shooting indoors, where the room is lit with traditional small light bulbs. When shooting outdoors, this setting will give your images a strong blue tint.

---

## Fluorescent

![](images/image05.png)

*A common icon for the fluorescent white-balance setting*

Use this setting when shooting indoors, where the room is lit with long, tube fluorescent lights. When shooting outdoors, this setting will give your images a blue tint.

---

## THINGS TO REMEMBER

 * You must manage white balance for good results.
 * Adjust your white balance if you move from one light type to another while shooting.
 * It is best to use a manual white-balance setting, but auto white balance is OK.

---

## QUIZ

#### 1.  Which best describes what white balance means?

1. The amount of light that strikes the sensor.
2. The color temperature of the light striking the sensor, ranging from very warm (yellow) to cool (blue).
3. The text color of the caption.
4. None of the above.

#### 2. TRUE or FALSE: Whenever possible you should always adjust the white balance in the camera or in post production.

1. TRUE
2. FALSE

#### 3. How can you control the white balance with your android smartphone or point and shoot camera? (It can be more than one)

1. Manually set the white balance to the icon that best represents the light color conditions under which you are shooting.
2. It is a good idea to calibrate with a white piece of paper as video journalists do.
3. Adjust the white balance in post production.
4. All of the above.

#### 4. How might you correct the white balance in the image shown here?

![](images/image04.jpg)

1. Change the white balance option to ’fluorescent’ when shooting.
2. Adjust the exposure.
3. Adjust the light temperature in post-production by adding yellow.
4. All of the above.

---

### QUIZ ANSWERS

<!-- more -->

1. 2
2. 1
3. 4
4. 3

---
