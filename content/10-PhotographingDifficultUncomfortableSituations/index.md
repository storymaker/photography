# 10. Photographing Difficult or Uncomfortable situations

![](images/image01.png)

## What do I do if someone is reluctant to be photographed or the circumstances are emotionally difficult for the subject or myself?

---

*Time to complete: 20-25 minutes*

* How to photograph difficult or sensitive topics
* How to recognize when your coverage makes people uncomfortable or may put them at risk
* How to recognize your own bias in your photo coverage
* How to work safely as a photographer
* How to decide the best way to represent a difficult topic in a photograph

---

## Tips for working safely as a photographer

People are sensitive to having their photo taken. The more difficult the circumstance of the story, the more difficult it is to get a photograph. Our own biases and emotional investment in a story can make it difficult to make good choices about shooting photos.

If you live in the community you cover, there are some unique challenges, such as:

* Should you take photographs of people if they don’t want you to?

* Should you publish very graphic images of violence and death?

* Should you help someone who is injured or photograph them?

Every photojournalist must make these choices for him or herself within the professional and ethical boundaries that journalism allows.

---

## Make people comfortable

In breaking news situations, journalists are typically working frantically and have little time to build rapport. But, when shooting event stories, issue stories, or profiles, you need to warm people up. Explain how the photo will be used. Tell people a bit about yourself. Speak to them conversationally. All of these things may help you get a photo of people who are reluctant to be photographed. You may find that using a smartphone makes some people more comfortable, while others may take you less seriously.

---

## Minimize harm to yourself and others

>[play video](https://storymaker.cc/appdata/lessons/en//4/4.11/3.mp4)
>Interview with a photographer who experienced photographing those affected by tragedy or grief.

>[play video](https://storymaker.cc/appdata/lessons/en//4/4.11/4.mp4)
>Interview with a photographer who experienced photographing those affected by tragedy or grief.

You must always be sensitive to the impact publishing images of people will have on those portrayed. Be sensitive when shooting those affected by tragedy or grief, especially children and people inexperienced with the media. Private people have a greater right to privacy than public officials and others who seek power, influence, or attention.

---

## Shoot for options

Even if a single image is going to be presented with a story or post, you want a variety of images from which to choose. In the case of difficult subject matter, images need to tell a story truthfully. They should not be gratuitous. When publishing graphic images, photographers need to consider the audience, the importance of the story, and the impact the images will have.

---

## Manage your bias

>[play video ](https://storymaker.cc/appdata/lessons/en//4/4.11/5.mp4)
>Interview with a photographer who found themself biased on an assignment

Know yourself. Be aware of your biases and do not let them influence how you portray a situation or issue. Good photojournalists seek to capture the universal human elements of stories. Humanity is not defined by national boundary, tribe, politics, or province.

---

## Your own safety

>[play video](https://storymaker.cc/appdata/lessons/en//4/4.11/6.mp4)
>Interview with a photographer about shooting in unsafe situations

Some situations are difficult to shoot because they are unsafe. No photograph is worth injury. When shooting in dangerous situations, always have an exit strategy. Be aware that soldiers and security forces may not wish to be photographed. Be smart about the risks.

Mental health is part of safety, too.

Only you can decide your limitations. Only you can decide if and when you should put down your camera and help. You will make a much better decisions if you think through your values before you find yourself in a high-pressure situation that requires you to make a decision on the spot.

Using a smartphone may encourage you to take greater risks, thinking you are less noticeable than shooters with large cameras. Be careful not to underestimate your level of risk.

---

## THINGS TO REMEMBER

* Explain your story to your subjects
* Work to make people comfortable with the process
* Be especially sensitive around tragedy
* Be balanced in your coverage no matter your own feelings
* Your own safety comes first

---

## QUIZ

#### 1. What are some things you can do to make subjects who are reluctant to be photographed more comfortable with the idea?

1. Explain to them how the photos are to be used.
2. Point out that it is an opportunity for them to speak out on a issue that might be important to them.
3. Negotiate a bit about how the photo will be taken and perhaps used.
4. All of the above.

#### 2. TRUE or FALSE: It is OK to have an agenda based on your own political and personal views when deciding how to cover an event.

1. TRUE
2. FALSE

#### 3. Which of the following are important to keep in mind when shooting under difficult circumstances (it can be more than one).

1. A photographer needs to be sensitive to the rights of private citizens when taking their photograph.
2. You should be aware of your own personal safety.
3. Don’t take pictures that are graphic and might upset someone.
4. All of the above.

---

### QUIZ ANSWERS

<!-- more -->

1. 4
2. 2
3. 1, 2

---
