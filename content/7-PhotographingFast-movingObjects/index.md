# 7. Photographing Fast-moving Objects

![](images/image11.png)

## How do I photograph fast-moving objects?

---

*Time to complete: 10-15 minutes*

 * Why fast-moving objects sometimes appear blurry
 * Techniques to freeze action, especially in low light
 * Using shutter priority mode
 * Using the flash

 ---

From football players to moving vehicles and marching protesters, journalists frequently have to take pictures of moving objects. A camera’s ability to freeze action is controlled by the shutter. If the shutter speed is too slow, it cannot adequately freeze action. The image appears blurry because the sensor is recording the movement. Shutter speeds are typically measured in fractions of a second. The faster the action, the faster the shutter speed needs to be.

![Shutter Priority Icon](images/image15.png "Shutter Priority Icon")

### Shutter Priority Icon

Some smartphones have settings that tell the camera to favor a faster shutter speed in the exposure equation. The icon for this is often a profile of a man running. For smartphones without this feature, there are several things you can do to help freeze action. This begins with knowing the limitations of the camera and being realistic about your choices.

---

## Managing Light

![](images/image14.jpg)

*An example of using daylight to light a subject*

The most common challenge when trying to photograph motion is inadequate light. There is not enough light to allow the camera to properly expose your picture *and* have a high enough shutter speed to freeze action. You can’t make the camera do the impossible. If you know you need to freeze action, plan your assignment so there will be enough light. This can mean choosing a time of day when there is sunlight. It might also mean choosing a location where there will be the most light possible — near a street light, stage light, or in the headlights of a car.

---

## Stabilize the camera

![](images/image09.png)

*The proper stance for holding a camera.*

Camera shake — the movement of your own hands while holding the camera — can cause or contribute to blurry images. This is true not only when shooting fast-moving objects. Always hold the camera close to your body. This is will make it easier to stabilize than if your arms are fully extended. It may help to rest the elbow of your camera hand on your knee or on an object to stabilize your camera.

---

## Consider Using the Flash

![](images/image13.jpg)

*An example of a photo taken with a phone’s flash*

Many mobile devices come with a built-in flash. The burst of light can help you freeze motion. Most phone flashes are only effective for about 3 meters.

---

## Pick your moment

![](images/image01.jpg)

*An example of capturing a moment.*

In many cases your subject will move at varying speeds. Footballers move more slowly as a play is being set up. Cars slow at corners or in traffic. Demonstrators are stationary for speeches. Choose a moment when things move more slowly. If you combine this with a good location for light, you can often get sharp pictures of difficult subjects even at night — demonstrators chanting under a streetlight, for example.

---

## Change your Angle to the Movement

![](images/image12.png)

*How to select the action feature on a mobile device*

The most difficult movement for the camera is when your subject is moving across the frame from right to left or left to right. A common technique for handling this situation is called panning. Point your camera and follow the subject as it goes by, twisting your body if you have to. This reduces the relative speed of the movement in relation to the camera. You can also change your angle so the subject comes at the camera on an angle instead of across the frame. This will allow the camera to track the object better and freeze the action.

The subject of your photo should always be in focus. Remember to manage the limitations of your equipment and the lighting conditions. Football players, vehicles and other moving objects may vary their speed. They might move into and out of greater light sources. Position yourself for the most advantageous moment to get your photo when the subject is well illuminated and moving comparatively slowly.

---

## THINGS TO REMEMBER

 * Recognize when there is not enough light to freeze action and adjust. Don’t just keep shooting blurry pictures and hope one will work.
 * When possible, plan for good light.
 * If your camera has an ‘Action’ preset for fast-moving objects, use it.
 * Stabilize your camera.
 * Pick the best moment to freeze action, typically when your subject is moving slowly.
 * Use the flash.

---

## QUIZ

#### 1. What causes blurry images when photographing fast-moving objects?

1. Too large an aperture.
2. A slow shutter speed.
3. Accidentally switching the camera to video mode.
4. Too high an ISO.

#### 2. TRUE or FALSE: Blurriness can be caused by handshake. It is important to stabilize the camera when trying to freeze action.

1. TRUE
2. FALSE

#### 3. How might you improve the sharpness of this image?

![](images/image08.jpg)

1. Wait for a moment when the subject is moving more slowly.
2. Zoom to a more telephoto setting.
3. Wait for a moment when the subject is in more light.
4. Ask the fast-moving subject to take a photo of himself.

---

### QUIZ ANSWERS

<!-- more -->

1. 2
2. 1
3. 1, 3

---
