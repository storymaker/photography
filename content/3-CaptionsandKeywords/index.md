# 3. Captions and Keywords

![](images/image12.png)

## How do I write a basic caption to accompany a photograph?

---

*Time to complete 15-20 minutes*

In this lesson you will learn:

* How to write a basic caption for a photograph
* How to write tags and keywords for a photograph

---

## Example Photo with a good caption, keywords, and tags.

![](images/2.jpg)

##### **Caption:** 10 June, 2012 — Tripoli, Libya — Football fans celebrate a 2-1 victory over Cameroon in World Cup qualifying match.

##### **Keywords:** Tripoli, Libya, Cameroon, World Cup, sports, Middle East

##### **Tags:** Sports, celebration, flags, World Cup, fans

Every photograph needs a caption and keywords/tags. A good caption provides the audience basic information and context to understand a photograph and its relevance to the news: where the photo was taken, when it was taken, and a very brief description of what’s in the photo. Search engines and search fields on sites like Flickr can’t recognize images. They can only recognize text associated with the image. Keywords and tags, along with the caption, help viewers find images through search engines.

---

## Example with a good & bad captions

![Demonstrators outside UN headquarters yesterday protest about peacekeepers in Darfur.](images/image3.jpg)

**Bad Caption:** Demonstrators outside UN headquarters yesterday protest about peacekeepers in Darfur.

**Good Caption:** 25 September, 2008 – New York, NY – More than 200 demonstrators gathered outside UN headquarters during its 63rd annual General Assembly meeting to protest what they say is a Security Council slow to act on its own demand for peacekeepers in Darfur. -SIPA Press/John Smock

---

Captions should be clear, accurate and complete in the information they provide about the photograph.

---

## Captions in Photo Essays

The captions for a photo essay should not repeat the same information over and over. Write captions as short, progressive explanations of the individual image in the context of the entire slideshow. In some cases you may present them as title cards that present short factoids. Factoids are additional information or statistics that help provide context.

![](images/image06.jpg)

**Caption: 27 April, 2006 — Birqash, Egypt — A camel merchant chases a camel at the camel market in Birqash, an hour northwest of Cairo. Sale of camel meat is on the rise in Egypt and surrounding countries in response to the bird flu epidemic, which has made chicken unsafe to eat. (John Smock/SIPA Press)**

![](images/6.jpg)

**Caption: 27 April, 2006 -- Birqash, Egypt -- A young boy helps load baby camels on to trailers at the Camel Market in Birqash, an hour northwest of the Cairo. Hundreds of animals make the trek from as far away as southern Sudan to what is perhaps oldest and largest remaining camel market in the Middle East. The animals are sold for meat as well as for transportation. (John Smock/SIPA Press))**

![](images/7.jpg)

**Caption: 27 April 2006 – Birqash, Egypt – Camels loaded on a truck bound for Cairo leave the camel market. Hundreds of camels are sold each week. These camels will be sold to butchers for meat. (John Smock/SIPA Press)**

---

## Writing captions

A caption should be one or two sentences. In most photo captions the first sentence identifies the people and location of the photograph and date when it was taken. The second sentence should provide contextual information to help the audience understand what they are looking at.

**The first sentence should:**

 * Be written in the present tense.
 * Be written as a complete sentence.
 * Describe what is in the picture.
 * Say exactly when it was taken.
 * Say exactly where it was taken.

**A second or sometimes third sentence:**

 * Typically can be written in the past tense if that makes more sense.
 * Should be written as a complete sentence.
 * Should provide context to allow the audience to better understand the significance of the image.

---

![](images/9.jpg)

**3 November, 2011 – Rustavi, Republic of Georgia – Extensive repairs begin on the Baku-Tbilisi-Ceyhan (TBC) pipeline conducted by British Petroleum (BP). When operational the pipeline transports as much as a million gallons a day, providing an alternative to fuel from Russia to countries in the Caucasus. (John Smock/SIPA Press)**

---

![](images/8.jpg)

**21 October, 2012, -- Cairo, Egypt – Egyptian street artist Ganzeer opened an exhibit of his artwork this week at a gallery in Cairo. Ganzeer was arrested for his work during the revolution. (Photo: John Smock)**

---

![](images/10.jpg)

**20 October, 2012 – Giza, Egypt – Vendors normally busy offering camel rides and tours at the pyramids sit idle. Tourism has been down significantly since the revolution. (Photo: John Smock)**

---

The exact format for captions can vary, but a basic photo caption should:

 * Clearly identify the people and location in the photo. Professional titles and the formal name of the location should be included. In large groups, identifications of only notable people may be required. At public events where individuals in the frame are not significant no I.D. may be required.

 * Include the date the photograph was taken. If an archive photograph or photograph taken prior to the event is used, the caption should make it clear that it is a “file photo.”

 * Provide some context or background for the audience members so they can understand the news value of the photograph.

 ---

## Keywords and tags

![Tags: tank, Russian Afghanistan. This image is licensed to be reproduce commercially.](images/image17.jpg "Tags: tank, Russian Afghanistan. This image is licensed to be reproduce commercially.")

**Tags: tank, Russian Afghanistan. This image is licensed to be reproduced commercially.**

Keywords and tags help search engines find images and work to identify photos in the archive. Typically there will be a field for keywords or tags along with the caption field.

---

### Tips for writing keywords and tags:

 * Include specific geographic information.
 * Avoid abbreviations.
 * Keyword with the kind of photo it is (portrait, demonstration, economy).
 * Consider word choice: Would someone use another word more naturally to do a search?
 * Think about related words to your topic that might help direct people to your photo.
 * Use common words that reflect specific subjects. For example, with computer security: hacking, hackers, security, firewall, attack, network, flaw.
 * The order of the keywords/tags is important; the first couple of words are valued more heavily.

 ---

## Tag Examples:

![education, Afghanistan, girls, school, English language](images/image08.jpg "education, Afghanistan, girls, school, English language")

**education, Afghanistan, girls, school, English language**

---

![flag, children, Libya, revolution, Arab spring](images/image07.jpg "flag, children, Libya, revolution, Arab spring")

**flag, children, Libya, revolution, Arab Spring**

---

![election, Arab, Arab-American, United States, politics, women, girls, vote, voting](images/image15.jpg "election, Arab, Arab-American, United States, politics, women, girls, vote, voting")

**election, Arab, Arab-American, United States, politics, women, girls, vote, voting**

---

## THINGS TO REMEMBER

 * Basic caption and keywords should always be included.

 * Make sure names of people and places are correctly identified and spelled.

 ---

## QUIZ

### 1. The first sentence of a photo caption should include

1. A brief description of the scene in the image, the date, the locations, and the names of any important people or landmarks.
2. A history of the event photographed.
3. The photographer’s interpretation of what people in the image are feeling.
4. All of the above.

### 2. TRUE of FALSE Keywords and tags not only helps people find your images online, they help you and news organizations find images more easily in your photo archive.

1. TRUE
2. FALSE

### 3. A good caption has which of the following characteristics (it can be more than one).

1. The location and date the image was shot is clear.
2. Significant people portrayed are identified by full name and title.
3. There are no misspelling or grammatical errors.
4. All of the above.

### 4. What is the best caption for this image?

![](images/image18.jpg)

1. 12 September, 2012 – Rustavi, Republic of Georgia — Interior Ministry troops search cars for suspected terrorists on election day along the main road leading to Tbilisi, the capital of this small Caucasian country.
2. Soldiers by the road on election day in the Republic of Georgia.
3. Angry soldiers loyal to the president in Georgia hunt for opposition party members.

---

### QUIZ ANSWERS

<!-- more -->

 1. 1
 2. 1
 3. 4
 4. 1

 ---
