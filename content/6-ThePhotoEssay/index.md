# 6. The Photo Essay

![](images/image16.png)

## What are some basic principles for shooting photographs that will work well together as a set?

---

*Time to complete: 15-20 minutes*

In this lesson you will learn:

* How to identify stories that might best be told with multiple images
* Different ways to structure stories told with multiple images
* How to structure a narrative-style photo essay
* How to structure a photo series

---

Most stories are accompanied by a single photograph. That photograph needs to summarize the event in a single frame.

![](images/image11.jpg)

>*New York City police officers check subway cars at Columbus Circle on Friday, Oct. 7, 2008. Security in the city’s mass transit system has been increased following yesterday’s announcement of a specific terrorist threat to the subway system. (AP Photo/John Smock)*

A photo essay that includes several images can be a powerful visual storytelling tool on its own, or with other story elements. Photo slideshows are among the most popular online content and engage large numbers of viewers.

![](images/image38.jpg)

>*About 20 students attend a course on media marketing at the DISC NGO school in Cairo, Egypt on Oct. 22, 2012. The school offers a variety of media training and software development courses. (John Smock/Small World News)*

![](images/4.jpg)

![](images/5.jpg)

![](images/6.jpg)

![](images/7.jpg)

![](images/8.jpg)

![](images/9.jpg)

![](images/10.jpg)

There are some key differences between photo essays and video. Viewers typically have a different relationship with photos — our brains are designed to remember still rather than moving images. Photo essays require less bandwidth and storage space than video. A photo essay can also be easier to produce than a well-sequenced video story.

---

## Example of a good photo essay

![](images/11.jpg)

>*Students attend a course at the all-boys Eagle Academy for Young Men in Queens, New York City, on April 12, 2006. The school is one of several small schools in the city public school system that is working to combat high dropout rates through smaller class size and a more tightly focused curriculum. (John Smock/Gates Foundation)*

![](images/12.jpg)

![](images/13.jpg)

![](images/14.jpg)

![](images/15.jpg)

![](images/16.jpg)

![](images/17.jpg)

---

## Example of a bad photo essay

![](images/image34.jpg)

>*Vendors at the Pyramids at Giza in Egypt sit idle due to so little business in October. The number of daily visitors recently is a fraction of the roughly 5,000 who arrived each day prior to the revolution.*

![](images/19.jpg)

![](images/20.jpg)

![](images/21.jpg)

![](images/22.jpg)

![](images/23.jpg)

![](images/24.jpg)

---

Every photo essay needs structure and a clear message. A photo essay can be structured one of two ways: as a narrative story or a series. Aim for 5-8 images in your final edit. You may need to shoot a lot more to get that many useable pictures.

---

## Narrative story

![](images/image33.jpg)

>*Pieces of pipe 10 meters in length for the Baku-Tblisi-Ceyan oil pipeline are manufactured in the Republic of Georgia and then shipped for assembly to locations along the pipeline’s path.*

![](images/26.jpg)

![](images/27.jpg)

![](images/28.jpg)

![](images/29.jpg)

![](images/30.jpg)

When planning a narrative story you should create a narrative arc — character, action, result, place and signature. . Think central question, climax and resolution. Think cause and effect. Your slideshow should tell the story visually as thoroughly as a written story tells it in words.

---

Photography’s greatest strength is in its ability to dramatically illustrate the universality of the human experience. Good narrative stories are built around a character or characters with whom viewers can identify. The audience need someone or something that they can identify with or that captures their imagination to get them into a story — this is your story hook. Identify what that is and it will help ensure your story has a tight, engaging focus.

---

There are several conventional ways to structure the narrative of a story. Have a clear idea what your essay is about and shoot the images you need to illustrate that. Every new image should contain new information that advances your story. The story types outlined below will help you do that. Sometimes photographers will use a combination of the options presented below.

---

## Highlights

![](images/31.jpg)

>*Georgians celebrate the official transfer of power from former President Eduard Shevardnadze to the new government on November 23, 2003 in Tbilisi, Georgia.*

![](images/32.jpg)

![](images/33.jpg)

![](images/34.jpg)

![](images/35.jpg)

![](images/36.jpg)

![](images/37.jpg)

![](images/38.jpg)

All photo stories are highlights stories; you should always seek to relay the most important visual elements of a story. But some stories are structured less to illustrate a clear storyline and more to show the peak moments or most dramatic aspects of the story.

---

## Process

![](images/38.jpg)

>*George Tanner mends clothes and makes suits as he has done for more than 30 years at his shop in Flatbush, New York, on Sept. 22, 2011.*

![](images/39.jpg)

![](images/40.jpg)

![](images/41.jpg)

![](images/42.jpg)

![](images/43.jpg)

A process photo essay shows how something is done from the beginning to end, for example, you could document how a sculpture is made, an election works, or even follow an arrest through its court case.

---

## Chronology

![](images/44.jpg)

>*Mohammed Assul spends his evening operating a faluka on the Nile River in Cairo, Egypt on Oct. 24, 2012. Business has been poor, he says, since the revolution last year. (John Smock/Small World News)*

![](images/45.jpg)

![](images/46.jpg)

![](images/47.jpg)

![](images/48.jpg)

![](images/49.jpg)

![](images/50.jpg)

Real or implied, you can let time structure your story. A typical way to structure a photo story through time is as a “day in the life” piece.

---

## The Slideshow

News organizations have developed a complex formula for the shots required for a good photo essay or slideshow. The formula is designed to help photographers avoid sameness and guarantee structure. A good slideshow moves “in” and “out.” As the audience moves through the story, the images move from tight to wide and back, over and over. This kind of rhythm keeps viewers interested and clicking on the next image.

Here is the formula for the different types of images that make a good slideshow. Make sure to shoot images in each of the categories to give your slideshow a clear structure; a variety of images tell a compelling story.

*NOTE: this formula is not always applied to highlights stories, which are often compilations of the best photographs of an issue)*

---

## Signature photo

![](images/image44.jpg)

*An example of a signature photo.*

A photo that summarizes the entire issue and illustrates essential elements of the story. If only one photo were to run, this would be the photo.

---

## Place shot

![Example of an Establishing photo](images/image17.jpg "Example of an Establishing photo")

*An example of a place photo.*

Use a wide-angle shot to establish the place where your story happens. When you do a photo story, you are taking viewers on a journey. Use this shot to give them a sense of where they are going, an image that allows them to understand the rest of the story in a geographic context.

---

## Close-up

![](images/image36.jpg)

*An example of a close-up photo*

A detail highlights a specific element of your story. Close-ups, sometimes called detail shots, don’t do a lot to inform the viewer but they do a great deal to dramatize a story.

ALWAYS shoot lots of close-ups.

---

## Character

![](images/image43.jpg)

*An example of a Character photo.*

This can be either a tight head shot or an environmental portrait, in a context relevant to the story. Photo essays are built around characters. You need to have good portrait that introduces viewers to the character. Always shoot a variety of portraits, some candids, meaning people are behaving naturally, and some posed.

---

## Action

![](images/image10.jpg)

*An example of an interaction photo*

Focus on your subject in a group during an activity. Images of your character interacting with others helps give a human dimension to your character.

---

## Result

![](images/image14.jpg)

*An example of a result photo*

A photo that can be used to close the story, one that says “the end.” This might be an image of shopkeeper closing his store, the setting sun as election observers go home, or a child in a hospital sleeping after surgery.

The first few images in a slideshow are especially important and often include a combination of the following:

 * **A place:** Often a wide-angle image to give a sense of location, a sense of where the story is happening.

 * **A character:** A photo story must be humanized quickly. You need to introduce your character as a traveling companion on your journey.

 * **A signature:** A telling shot early on that is graphically appealing and visually summarizes the story.

 ---

## The series

![](images/image29.jpg)

>*Murals portraying the fall of General Muammar Gaddafi and the nation’s aspirations in the aftermath of the revolution cover walls throughout Tripoli, Libya on June 8, 2012.*

![](images/image39.jpg)

![](images/image37.jpg)

![](images/image35.jpg)

![](images/image12.jpg)

![](images/image20.jpg)

![](images/image19.jpg)

![](images/image18.jpg)

The series is a set of similar images designed to illustrate a comparative point: for example a series of portraits or of destroyed buildings or a particularly fashionable type of sunglasses. Images in a series should be stylistically similar to further illustrate the comparison. A series can be a great way to illustrate an idea on its own or as a sidebar element to a video, audio, or text-only story.

---

You don’t need to make every image in series exactly like the others. Sometimes it’s just not possible. But here are a few things to keep in mind:

* Angle of view:When possible, try to keep the angle of view consistent in a series, for example, if one picture is taken from eye-level, take them all from eye-level.

* Focal length: Try to be consistent in the distance from your lens to your subjects. This will ensure a consistent perspective.

* Framing: All images should be framed in about the same way. If focal length stays the same, you may need to step farther away for larger objects (or people with bigger heads) and closer for smaller object.

Color and image quality: If possible, avoid using a flash with some images and not others. Be consistent with ISO and white balance

---

## THINGS TO REMEMBER

 * Shoot lots of images.
 * Variety is key.
 * Think through the images you will need to portray a complete story.
 * Plan your coverage in advance.

---

## QUIZ

#### 1. Good photo essays are more than a random assortment of images. They need a story structure. Which of the options below might provide structure to a story (it can be more than one)?

1. The story could be structured chronologically.
2. The story could be structured as process piece to show how something is done.
3. The story could be structured as a highlights piece that illustrates the most important elements of the subject.
4. All of the above.

#### 2. TRUE or FALSE: A narrative story needs variety. A series on the other hand works better if the composition is consistent from image to image.

1. TRUE
2. FALSE

#### 3. Good stories typically have the following characteristic (it can be more than one).

1. The final project will be between 8 and 20 images.
2. If it’s a narrative story, there will be visual variety.
3. Stories about broad issues are always better then highly focused stories.
4. All of the above.

---

### QUIZ ANSWERS

<!-- more -->

1. D
2. A
3. A, B

---
