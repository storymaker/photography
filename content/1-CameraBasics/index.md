# 1. Camera Basics

![](images/image11.png)

## How does a digital camera work?

*Time to complete: 15-20 minutes*

---

In this lesson you will learn:

 * How the camera in your mobile device works
 * What shutter, aperture, and ISO are
 * An introduction to the exposure triangle

---

![](images/image15.png)

A big part of understanding how a camera works and how to use it is to understand what it does not do well compared to the human eye. Your eye can see details in places with extreme contrast — highlights on a snow-covered mountain top, for example, and dark shadows in a cave entrance in the side of that same mountain. Digital cameras cannot always see details visible to your eyes. Images often appear too bright or too dark.

Your eye is quick to focus on the singular thing of interest in your field of view. A digital camera must be told where to focus, especially in low light, and can be slow to focus.

![Example of a photo underexposed in low light](images/image13.jpg "Example of a photo underexposed in low light")

Example of a photo underexposed in low light.

---

![Example of a photo exposed correctly in low light](images/image19.jpg "Example of a photo exposed correctly in low light")

Example of a photo exposed correctly in low light.

---

A camera must be told to freeze motion and struggles to do it with fast-moving objects, especially in low light. It takes much more light to record an image with a mobile device than for your eye to see it.

---

![How a camera works](images/image18.png "How a camera works")

How a camera works.

A digital camera is very simple. It has a digital sensor that collects light, a shutter button that controls how long the sensor is exposed to light, and a lens to control the amount of light that strikes the sensor. Here is a brief explanation of each.

## The sensor

![How a camera sensor accepts light. A. Incoming light, B. Color Filters, C. Light is converted to electricity, D. Sensor is made up of millions of pixels that collects light](images/image12.png "How a camera sensor accepts light. A. Incoming light, B. Color Filters, C. Light is  converted to electricity, D. Sensor is made up of millions of pixels that collects light")

How a camera sensor accepts light.

* A. Incoming light;
* B. Color filters;
* C. Light is converted to electricity;
* D. Sensor is made up of millions of pixels that collect light.

Inside every digital camera is a sensor that records light. These sensors come in a variety of shapes, sizes, and designs. Larger sensors are better at recording good color and sharp images, especially in low light.

![best ISO usage for different environments](images/image17.png "best ISO usage for different environments")

ISO usage for different environments

Digital sensors can be set at different record levels, increasing or decreasing their sensitivity to light. This system for adjusting the sensor’s sensitivity is called the ISO and is measured in numeric range, typically from 100 to 800 and higher on mobile device. Your ISO should be higher in low light (800) and lower in bright light (100). Your camera will set the ISO automatically. But many Android devices and camera apps will let you set it manually.

---

## The shutter

When you push the shutter button, the camera activates the sensor. This controls the length of time that light is absorbed by the sensor. This is called the shutter speed. In photography this is measured in fractions of a second. The shutter allows photographers to freeze action. If the shutter is too slow, the subject’s movement may be recorded by the sensor, making it appear blurry. You may not have the full range of shutter functionality on your mobile device.

Most mobile devices will not let you directly control the shutter. But many Android camera and camera apps will offer a preset for action photos that works by favoring a faster shutter speed in the exposure calculation.

---

## Aperture

![How a camera aperture works](images/image14.png "How a camera aperture works")

*How a camera aperture works*

The aperture is a small valve or iris that controls the amount of light that striking the lens of the camera. The larger the valve opening, the more light that will strike the sensor. The smaller the iris valve, the less light striking the sensor.

---

## Balancing the exposure triangle

Creating an image with a proper exposure is very much a game of playing ISO, aperture, and shutter off of one another to achieve the desired effect. More expensive cameras provide the photographer more manual control over these features, but even smartphones now let you adjust exposure by changing the ISO or touching the screen at a point that represents a medium exposure value. Some also have digital zoom that allow you to zoom in or out.

![](images/image16.jpg)

*Example of an unbalanced photo.*

---

![](images/image10.jpg)

*Example of an balanced photo.*

---
## Working within limitations

Understanding these limitations and working within them is the key to getting the most out of your smartphone camera. Remember, if you have a smartphone and you need photos for your story, plan your coverage around time of day or lighting conditions if possible.

![Example of an unusable photo](images/image02.jpg "Example of an unusable photo")

*Example of an unusable photo*

![](images/image12.jpg)

*Example of a balanced photo*

Additionally, while the image of ambulances speeding by might be a great one for your story, you may opt to also take a more static picture of the front of the hospital to make sure you have a usable image in case the ambulance photo doesn’t work out.

---

# THINGS TO REMEMBER

 * Smartphone cameras is that phone cameras are limited in what they can do compared to larger point-and-shoots and certainly dSLRs.
 * You do not have the same degree of manual control with a smart phone camera as you do with a point-and-shoot or dSLR.
 * Smartphone cameras are difficult to use in low light.
 * Smartphone cameras sometimes struggle with fast movement.

---

## QUIZ

#### 1. The camera is limited in its ability to collect light in which of the following ways:

1. It requires more light to expose a scene than it takes your eye to see it.
2. The camera struggles to freeze action where your eye (and brain) do it easily.
3. You eye can decide and adjust focus quickly; a camera is slower and needs specific instructions about where to focus.
4. All of the above

#### 2. True or false: Smartphone image quality is fundamentally the same as many point-and-shoot and pro cameras. The difference is that you may have no manual control of exposure, and the lens and aperture within are very small.

1. True
2. False

#### 3. What happens when you push the shutter on a smartphone camera?

1. It takes the picture.
2. It focuses the image.
3. It decides the exposure.
4. All of the above.

 ---

### QUIZ ANSWERS

<!-- more -->

1. 4
2. 1
3. 4

---
