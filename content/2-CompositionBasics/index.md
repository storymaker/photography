# 2. Composition Basics

![](images/image15.png)

## What are some basic principles of good photo composition?

---

*Time to complete: 10-15 minutes*

In this lesson you will learn:

 * Key concepts for photo composition
 * What the rule of thirds means in photography
 * How to create layering in a photograph
 * What ‘working the edges’ means
 * How to capture peak action
 * Tips for taking photographs safely

---

Photography borrows a lot from painting. The best photos are composed so the subject of the image is framed well and the purpose of the photo is clear. Good color and the relationship between the dominant shapes in a frame — the elements of the picture your eyeis drawn to first — are important to good composition. How light is used is extremely important.

![Example of a photo that provides context and action](images/image06.jpg "Example of a photo that provides context and action")

*Example of a photo that provides context and action.*

When taking photographs of a news event, it’s important to remember that your audience members can’t see the totality of the scene as you can. They can’t know what you don’t show them.

![](images/image03.jpg)

Good photojournalists capture not just the drama and action of an event in their images but include visual elements that provide context as to the location and the causes of the event being photographed. The world can look very different in a photograph. This requires you to think visually, which can be a challenge for even the best journalists.

---

## Rule of thirds

![Rule of Thirds](images/image05.png "Rule of Thirds")

*Rule of thirds grid*

The foundation of good composition is the rule of thirds. The rule of thirds breaks down the frame into nine equal rectangles. Many smartphones will allow you to display the rule of thirds grid over the camera display.

### Rule of thirds examples

![](images/image11.jpg)

Good photographers compose along the intersections of lines in the frame and work to use each of the nine rectangles to tell their stories.

![](images/image12.jpg)

Amateurs tend to place the subject in the middle of the frame and ignore the remaining space rather than use it to make a more compelling image.

![](images/image12a.jpg)

![](images/image12b.jpg)

---

## Layering

![Example of a photo with a clear foreground, middleground and background](images/image13.jpg "Example of a photo with a clear foreground, middleground and background")

*Example of a photo with a clear foreground, middleground, and background*

Another important principle for good composition is called layering. Creating distinct layers in your frame — foreground, middleground, and background — helps you tell more complex visual stories with your photography. You might choose to place your subject, a farmer whose fields are barren because of drought, frame left in the foreground and his dried up empty field in the background. This will give the image a clear subject layer (foreground) and context (background layer).

---

## Working the edges

![Example of a photo using edges](images/image04.jpg "Example of a photo using edges")

*Example of a photo using edges*

In a well-composed photograph, important shapes or information complete themselves in the frame. The cook’s hands are not cut off awkwardly. The personnel carrier who provides context to the picture in the background layer isn’t chopped in half at the edge of the frame. Photographers call this working the edges. Shapes such as door frames or trees or objects with strong color are used to define the edge of the frame by allowing them to resolve within it.

---

## Peak action

> [watch video on the web](https://storymaker.cc/appdata/lessons/en//4/4.2/11.mp4)
> *Interview with a photographer who discusses using composition on assignment.*

A final key element to good photography is timing. The most powerful photographs are taken at the split second when all the compositional elements come together and the action in the scene is at its most dramatic. This is called the moment of peak action. The best photographers have great anticipation. They will often set up to take advantage of key compositional elements and then wait for the moment of peak action.

A little care in composition and thoughtfulness about the information you need to convey to tell the story will go a long way toward making better photographs.

---

## Safety

![](images/image08.png)

Always remember to consider your safety, and that of your subjects, first. A reporter can stand at the edge of a potentially dangerous scene. A photographer needs to get close to take a good picture. News photographer are always balancing safety against the need to capture compelling images.

---

# THINGS TO REMEMBER

 * Think visually.
 * Organize the elements of your picture to tell a compelling and informative story.
 * Invest yourself in taking better photographs. Good photography takes time.
 * You need to plan and think through how you will get images that capture the essence of a story and move viewers emotionally.

 ---

## QUIZ

#### 1. What does thinking visually mean? (There may be more than one answer.)

1. Planning your story from the outset for good visual content
2. Devoting time to getting good images
3. Understanding how telling visual stories is more than merely translating what you would report and write
4. None of the above

#### 2. True or false: A picture should be well organized. A good way to do this is to have only a single, posed subject and put your subject in the middle of your frame.

1. True
2. False

#### 3. Which of the following is a basic principle of photography for good composition? (There may be more than one answer.)

1. The rule of thirds
2. Working the edges
3. Layering
4. None of the above

#### 4. Which of these images is properly exposed?

1. ![image A](images/quiz_3_a.jpg "image A")
2. ![image B](images/quiz_3_b.jpg "image B")
3. ![image C](images/quiz_3_c.jpg "image C")

---

### QUIZ ANSWERS

<!-- more -->

1. 1, 3
2. 2
3. 1, 2, 3,
4. 2

---
