# 5. Post Production

![](images/image08.png)

## What are the technical requirements and ethical limitations when editing my photos to ensure high quality, useable images?

---

**Time to complete: 10-15 minutes**

In this lesson you will learn:

 * The principles of good photo editing
 * How to crop an image
 * How exposure correction works
 * How white balance correction works

 ---

Your initial photo is the same as a rough draft of a written story. Images can be dramatically improved by how they are cropped, sized, and color corrected.

---

![Example of a properly exposed and focused photo](images/image10.jpg "Example of a properly exposed and focused photo")

*Example of a photo that is properly exposed and in focus.*

Editing well begins with shooting well. Make sure your images are properly exposed and focused. Images that are technically strong to begin with are easier to edit. The ability to edit your own work is an unacknowledged but essential part of the job.

---

![](images/3.jpg)

*Example of a photo that's been enlarged and pixelated.*

Sizing, cropping, color correcting, and adding text takes time, so you need to plan. If time is very important, publish your best images as-is and provide updated versions later in your coverage.

---

## Cropping

Cropping can turn a mediocre shot into a powerful image. Crop ruthlessly with the rule-of-thirds in mind. Cut excess or meaningless space from the frame.

![Before Cropping](images/image12.jpg "Before Cropping")

*Before cropping.*

Cut objects from your frame that distract the viewer and diminish impact. The entire frame should work to tell the story.

![](images/5.jpg)

*After cropping*

---

## Let Objects Complete

Be careful about cropping people at awkward points; do not cut off hands and feet. Do not amputate. Let important shapes complete themselves in the frame.

![Before Cropping](images/image09.jpg "Before Cropping")

*Before cropping.*

 Shapes draw the audience’s eye into the picture; a completed shape helps keep it there.

 ![](images/7.jpg)

---

## Sizing

![](images/image07.png)

Even mobile devices are capable of creating images with resolution so great that they can be published in a magazine. Always shoot on the largest file size available. However, large files take up a lot of space and are slow to send. Some mobile devices and apps will allow you to make an image smaller for more efficient reproduction on a website. ALWAYS keep a copy of the original images if reducing file size dramatically.

---

## Exposure and white balance correction

Editing apps such as Photoshop Express or social apps such as Instagram will allow you to increase or decrease exposure and correct color problems, called white balance. They will also allow you to correct images that appear too yellow or too blue. This may be as simple as adding a “darken” filter for overexposed images or an “indoor” filter for images that are very yellow. Images with proper exposure and white balance when shot should require little more than a tweak, but images shot under difficult lighting may be dramatically improved in post-production.

---

## Unethical manipulation

Photo editing tools are very powerful. Reporters should not make up facts or quotes, and photojournalists should not alter images.

![](images/12.jpg)

*Example of an unethical photo manipulation*

![](images/13.jpg)

*Example of an unethical photo manipulation*

---

The Associated Press ethical guidelines for post-production of photographs provide a good code of ethics to follow and include:

 * The content of a photograph must not be altered substantially in PhotoShop or by any other means.

 * No element should be digitally added to or subtracted from any photograph.

 * The faces or identities of individuals must not be obscured by an editing tool except as a last resort to protect safety.

 * Only retouching or the use of the cloning tool to eliminate dust and scratches is acceptable.

 * Minor adjustments with editing tools are acceptable. These include cropping, dodging and burning, conversion into grayscale, and normal toning and color adjustments that should be limited to those minimally necessary for clear and accurate reproduction (analogous to the burning and dodging often used in darkroom processing of images) and that restore the authentic nature of the photograph.

 * Changes in density, contrast, color, and saturation levels that substantially alter the original scene are not acceptable.

 * Backgrounds should not be digitally blurred or eliminated by burning down or aggressive toning.

 ---

## THINGS TO REMEMBER

 * Images that are technically strong to begin with are easier to manage in post-production.
 * Always shoot the largest file size your camera will allow. You can make smaller copies later.
 * A good crop can dramatically improve an image.
 * Correcting exposure, color and white balance can improve an image.
 * Anything beyond minor adjustments is unethical.

---

## QUIZ

#### 1. When shooting which of the following will make post production more effective?

1. Make sure your camera is set to the largest file size.
2. Make sure the image is properly exposed.
3. Make sure the image is properly focused.
4. All of the above.

#### 2. TRUE or FALSE 300 dpi (dots per inch) means you should file your images at 300 dots (or pixels) along the longest edge.

1. TRUE
2. FALSE

#### 3. Which of the following are ethical?

1. Distracting people can be removed from a frame with an airbrush or cloning tool.
2. Color and saturation can be made more intense in the interest of making the image emotionally stronger.
3. The image can be captioned with information that changes the meaning of a photograph.
4. None of the above.

#### 4. Which of the three photos is unethically edited?

1. ![](images/image13.jpg)
2. ![](images/15.jpg)
3. ![](images/16.jpg)

---

### QUIZ ANSWERS

<!-- more -->

1. 4
2. 1
3. 4
4. 3

---
