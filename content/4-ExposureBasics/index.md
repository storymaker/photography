# 4. Exposure Basics

![](images/image12.png)

## Why are my images overexposed (too light) or underexposed (too dark), and how do I fix it?

---

*Time to complete: 10-15 minutes*

In this lesson you will learn:

 * How your camera measures light
 * How to diagnose common problems like low light and backlighting
 * How exposure compensation works

 ---

Inside every mobile camera there is a light meter. This meter takes a light reading in the instant before you take a picture and tells the camera how to set the ISO, aperture, and shutter (see the camera basics section). With point-and-shoot cameras and dSLRs this meter is more sophisticated, and it is up to the photographer to set the exposure in whole or part based on the light reading.

---

![Example of common problems with light metering](images/image05.jpg "Example of common problems with light metering")

*Example of common problems with light metering*

These meters are easily confused. They take an overall reading of light coming into the lens and average it to set the exposure. The camera will have difficulty estimating exposure if there is a lot of variation in the available light — mobile cameras cannot expose well for images with a lot of variation between highlights and shadows.

Insufficient light is the most common problem photographers confront. A camera is a poor light-gathering device compared to the human eye. One of the biggest challenges photographers have when shooting in low light is recognizing the problem and methodically solving it rather than taking a bunch of images and hoping one will work out.

---

![Example of a photo taken with a camera phone](images/image13.jpg "Example of a photo taken with a camera phone")

*Example of a photo taken with a camera phone*

![](images/4.jpg)

*Example of a photo taken with a point and shoot camera*

![](images/5.jpg)

*Example of a photo taken with a DSLR camera*

In low-light situations, there is not enough light striking the sensor to properly expose it. Smartphones have smaller lenses and thus much smaller maximum apertures (see the camera basics section) than professional cameras. Because the aperture is so small, they can’t let in as much light as larger cameras with larger lenses operating at the same shutter speed.

---

Other exposure problems may arise when shooting in low-light conditions because the camera tries to make the scene appear lighter than it actually is.

![Example of a backlit photo](images/image10.jpg "Example of a backlit photo")

*Example of a backlit photo.*

![](images/7.jpg)

*Example of a light bouncing off a highly reflective surface.*

---

One common issue is backlighting. You are shooting a subject in relative shadow with bright light or sunshine behind the subject. This can trick the meter, and the subject will appear backlit.

Another common problem is light bouncing off of a highly reflective surface, such as white tent canvas, snow, or sand.

![Example of an over exposed photo composition](images/image04.jpg "Example of an over exposed photo composition")

*Example of an over-exposed photo*

![Example of how to properly expose a dark arear with a smartphone](images/image14.jpg "Example of how to properly expose a dark arear with a smartphone")

*Example of how to properly expose a dark area with a smartphone*

Many smartphone cameras allow the photographer to touch a place on screen to tell the camera where to draw its exposure value. Some devices will also allow you to point the camera at a shadowy area in the frame, touch the screen and reframe the shot.

---

![Example of how to properly adjust a photo](images/image06.jpg "Example of how to properly adjust a photo")

*Example of how to properly adjust a photo’s exposure using exposure compensation*

Some smartphone cameras and photo apps will allow you to adjust exposure using a feature called “exposure compensation.” You can tell the camera to adjust the metered exposure to overexpose or underexpose the image before you take the photo. This can help compensate for excessive highlights or shadow areas in the frame.

Images that are slightly overexposed or underexposed can sometimes be fixed in post-production with built-in photo software apps or apps such as Adobe Photoshop Express. Adobe Photoshop Express allows you to manage exposure in your picture. You can touch the face of a person in your photo and the camera will reset exposure for his/her face.

---

# THINGS TO REMEMBER

 * It is essential that your images be properly exposed.
 * Smartphone cameras often struggle to take well-exposed images under tough lighting conditions.
 * Managing the exposure within the range that the camera is capable can help a lot.
 * Always take a few practice pictures to check the exposure, and make adjustments if necessary.

 ---

## QUIZ

#### 1. What three things control exposure?

1. ISO
2. Aperture
3. Zoom setting
4. Shutter

#### 2. True or false: The shutter controls the amount of time the shutter is exposed to light.

1. True
2. False

#### 3. When shooting with a smartphone, you can do which of the following if the image is overexposed:

1. Nothing. Smartphones offer no manual control of exposure.
2. Many smartphone cameras now let you select the area of the frame that will be properly exposed by touching it. The camera will recalibrate exposure based on the spot you select.
3. Set the smartphone to manual camera mode.
4. None of the above.

#### 4. How might you correct the exposure in this example image?

![](images/image15.jpg)

1. Increase the ISO.
2. Adjust the exposure slightly in post-production.
3. Open the aperture (if possible).
4. Slow shutter speed being careful not to slow it so much that the image becomes blurry.
5. All of the above.

---

### QUIZ ANSWERS

<!-- more -->

 1. 1, 2, 3
 2. 2
 3. 2
 4. 5

---
